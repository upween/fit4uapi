const express = require('express'),
  app = express(),
  bodyParser = require('body-parser'),
  port = process.env.PORT || 3003
  ;
var cors = require('cors');
import jwt from 'jsonwebtoken';
import config from './config.json'
const sql = require('mssql');
import tokenValidator from './middleware/TokenValidator' ;
// connection configurations
const mc = sql.connect({
  
  user:'sa',
  password:'sql@2008',
 server:'182.70.254.93', 

  database:'Fit4UProduct',
  options: {
		encrypt: false,
	}
});
 
// connect to database
//mc.connect();
app.use(cors());
app.listen(port);

console.log('API server started on: ' + port);

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var routes = require('./app/routes/approutes'); //importing route
 
routes(app,{mergeParams: true})

app.use(tokenValidator);
app.get('/health', function (req, res) {
  res.status(200).send({status:true})
})
app.post('/generateToken' ,(req,res)=>{
let tokenObject ={
    userId:req.body.userName,
    name:"some name"
  }
var refreshToken = jwt.sign(tokenObject, config.refreshToken.secret, {
  expiresIn: config.refreshToken.expiresIn 
});
var token = jwt.sign(tokenObject, config.token.secret, {
  expiresIn: config.token.expiresIn 
});
res.status(200).send({
  error: false,
  result: {
    token,
    refreshToken
  }
})
})
app.post('/secured/validateToken' ,(req,res)=>{
  let token = req.headers["authorization"]
    jwt.verify(token, config.token.secret, (err, decoded)=> {
      if (err) {
          if(req.body.refreshToken){
            jwt.verify(req.body.refreshToken,config.refreshToken.secret,(err,decoded)=>{
                if(err)
                     return res.status(401).send({"error": true, "message": 'Failed to authenticate token.' });
                else
                    {   
                        delete decoded["exp"];
                        delete decoded["iat"];
                        var token = jwt.sign(decoded, config.token.secret, {
                            expiresIn:config.token.expiresIn 
                          });
                        res.status(401).send({error:true , data :{token},message:'New token generated'})  
                    }
            })
          }
          else
          return res.status(401).send({"error": true, "message": 'Failed to authenticate token.' });
      }
      else{
          req.body?req.body["tokenDetails"] = decoded:null
          res.status(200).send(decoded)
      }
  });
  
  })