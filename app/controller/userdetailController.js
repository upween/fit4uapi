'use strict';
var UserDetail = require('../model/userdetailModel.js');


  exports.create_a_userdetail = function(req, res) {
    var Insupd_UserDetail= new UserDetail(req.body);
   
    UserDetail.createuserdetail(Insupd_UserDetail, function(err, userdetail) {
        if (err){
          res.send(err);
        }
          else{
        res.json(userdetail);
          }
      });
    
  };

  exports.read_a_userdetail= function(req, res) {
  
  
    UserDetail.getuserdetail(req.params, function(err, userdetail) {
      if (err){
        res.send(err);
      }
        else{
     
        res.json(userdetail);
        }
    });
  };


  exports.read_a_userdetailbyid= function(req, res) {
  
  
    UserDetail.getuserdetailbyid(req.params, function(err, userdetailbyid) {
      if (err){
        res.send(err);
      }
        else{
     
        res.json(userdetailbyid);
        }
    });
  };
  exports.read_a_usercartbyid= function(req, res) {
  
  
    UserDetail.getUserCartbyId(req.params, function(err, cartitems) {
      if (err){
        res.send(err);
      }
        else{
     
        res.json(cartitems);
        }
    });
  };