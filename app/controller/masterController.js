'use strict';
var Masters = require('../model/masterModel.js');


  
  exports.read_a_category= function(req, res) {
  
  
    Masters.getCategory(req.params, function(err, category) {
      if (err){
        res.send(err);
      }
        else{
     
        res.json(category);
        }
    });
  };
  exports.read_a_brand= function(req, res) {
  
  
    Masters.getBrand(req.params, function(err, brand) {
      if (err){
        res.send(err);
      }
        else{
     
        res.json(brand);
        }
    });
  };
  exports.read_a_goals= function(req, res) {
  
  
    Masters.getGoals(req.params, function(err, goals) {
      if (err){
        res.send(err);
      }
        else{
     
        res.json(goals);
        }
    });
  };
  exports.read_a_flavour= function(req, res) {
  
  
    Masters.getFlavour(req.params, function(err, flavour) {
      if (err){
        res.send(err);
      }
        else{
     
        res.json(flavour);
        }
    });
  };