'use strict';
var ProductDetails = require('../model/ProductDetailsModel.js');


  exports.create_a_ProductDetails = function(req, res) {
    var Insert_ProductDetails= new ProductDetails(req.body);
   
    ProductDetails.createProductDetails(Insert_ProductDetails, function(err, productdetails) {
        if (err){
          res.send(err);
        }
          else{
        res.json(productdetails);
          }
      });
    
  };
  exports.read_a_maxproductid= function(req, res) {
  
  
    ProductDetails.getmaxproductid(req.params, function(err, productid) {
      if (err){
        res.send(err);
      }
        else{
     
        res.json(productid);
        }
    });
  };

  exports.create_a_UserCartItems = function(req, res) {
    var Insupd_UserCartItems= new ProductDetails(req.body);
   
    ProductDetails.createUserCartItems(Insupd_UserCartItems, function(err, UserCartItems) {
        if (err){
          res.send(err);
        }
          else{
        res.json(UserCartItems);
          }
      });
    
  };


  exports.read_a_productdetails= function(req, res) {
  
  
    ProductDetails.getproductdetails(req.params, function(err, productdetails) {
      if (err){
        res.send(err);
      }
        else{
     
        res.json(productdetails);
        }
    });
  };

  exports.read_a_ProductByGoalsAndBrand= function(req, res) {
  
  
    ProductDetails.getProductByGoalsAndBrand(req.params, function(err, ProductByGoalsAndBrand) {
      if (err){
        res.send(err);
      }
        else{
     
        res.json(ProductByGoalsAndBrand);
        }
    });
  };

  exports.read_a_productbyid= function(req, res) {
  
  
    ProductDetails.getProductbyid(req.params, function(err, productbyid) {
      if (err){
        res.send(err);
      }
        else{
     
        res.json(productbyid);
        }
    });
  };
  exports.delete_a_usercartbyid= function(req, res) {
  
  
    ProductDetails.deleteCartItems(req.params, function(err, cartitems) {
      if (err){
        res.send(err);
      }
        else{
     
        res.json(cartitems);
        }
    });
  };
  exports.read_a_latestproduct= function(req, res) {
  
  
    ProductDetails.getlatestproduct(req.params, function(err, product) {
      if (err){
        res.send(err);
      }
        else{
     
        res.json(product);
        }
    });
  };