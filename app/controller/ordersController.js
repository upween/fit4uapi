'use strict';
var Orders = require('../model/ordersModel.js');


  exports.create_a_orders = function(req, res) {
    var Insupd_Orders= new Orders(req.body);
   
    Orders.createorders(Insupd_Orders, function(err, orders) {
        if (err){
          res.send(err);
        }
          else{
        res.json(orders);
          }
      });
    
  };


  exports.read_a_orders= function(req, res) {
  
  
    Orders.getorders(req.params, function(err, orders) {
      if (err){
        res.send(err);
      }
        else{
     
        res.json(orders);
        }
    });
  }
  exports.read_a_orderbyuser = function(req, res) {
  
  
    Orders.getorderbyuserid(req.params, function(err, orders) {
      if (err){
        res.send(err);
      }
        else{
     
        res.json(orders);
        }
    });
  };
  exports.read_a_orderInvoice = function(req, res) {
  
  
    Orders.getorderInvoice(req.params, function(err, orderInvoice) {
      if (err){
        res.send(err);
      }
        else{
     
        res.json(orderInvoice);
        }
    });
  };