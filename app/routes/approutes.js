'use strict';
import TokenValidator from '../../middleware/TokenValidator'
import Axios from 'axios';
module.exports = function (app) {
  var productdetails =require('../controller/ProductDetailsController');
  var masters =require('../controller/masterController');
  var mobileverification =require('../controller/mobileverificationController');
  var userdetail =require('../controller/userdetailController');
  var orders =require('../controller/ordersController');
  var rolemenu =require('../controller/rolemenuController');





//Product Details Routes---------------------------------------------------
  app.route('/productdetails').post(productdetails.create_a_ProductDetails);
  app.route('/getmaxproductid').get(productdetails.read_a_maxproductid);
  app.route('/UserCartItems').post(productdetails.create_a_UserCartItems);

  app.route('/productdetails/:ProductID').get(productdetails.read_a_productdetails);
  app.route('/ProductByGoalsAndBrand/:Id/:ParentID/:Type').get(productdetails.read_a_ProductByGoalsAndBrand);

  app.route('/Productbyid/:ProductID').get(productdetails.read_a_productbyid);

  app.route('/deletecartitems/:UserID/:UniqueID').get(productdetails.delete_a_usercartbyid);
  app.route('/latestproduct/:ProductID/:ApplicationID/:Limit').get(productdetails.read_a_latestproduct);

//Masters Routes-------------------------------------------------------------
app.route('/categories/:CategoryID').get(masters.read_a_category);
app.route('/brands/:BrandID').get(masters.read_a_brand);
app.route('/goals/:GoalID').get(masters.read_a_goals);
app.route('/flavour/:FlavourID').get(masters.read_a_flavour);

app.route('/MobileVerification').post(mobileverification.create_a_mobileverification);
app.route('/RoleMenu/:ApplicationID').get(rolemenu.read_a_rolemenu);


//Order Routes
app.route('/Orders').post(orders.create_a_orders);
app.route('/Orders/:OrderID').get(orders.read_a_orders);

app.route('/orderbyuser/:UserID').get(orders.read_a_orderbyuser);

app.route('/invoice/:OrderID').get(orders.read_a_orderInvoice);

//user  Routes--------------
app.route('/usercart/:UserID').get(userdetail.read_a_usercartbyid);
app.route('/userdetailbyid/:UserID').get(userdetail.read_a_userdetailbyid);

app.route('/UserDetail').post(userdetail.create_a_userdetail);
app.route('/UserDetail/:UserDetailID').get(userdetail.read_a_userdetail);


//Send SMS AJAX--------------------------------------------------------------

app.get('/sendsms',(req,res)=>{
  let url=req.url;
   let parameter = url.split("?")[1];
let data =JSON.parse(decodeURI(parameter));
console.log(data);
Axios({
    method: 'post',
    url: 'https://api.msg91.com/api/v2/sendsms',
    data: data,
    headers:{
        "authkey":"264840AT4bfsGwDs5c74e909",
        "Content-Type":"application/json"
    }
  }).then((response)=>{
    console.log(response);
    res.status(200).send({
      status:"success"
      })
  }).catch((err)=>{
    console.log(err)
    res.status(500).send({
      "data":"",
      "err":"some error"
    })
  })
});
}