'user strict';
var sql = require('./db.js');
//Category object constructor -- category_master
var   UserDetail = function(userdetail){
    this.UserDetailID = userdetail.UserDetailID ;
    this.FirstName= userdetail.FirstName ;
    this.LastName = userdetail.LastName ;
    this.EmailAddress = userdetail.EmailAddress ;
    this.Address= userdetail.Address ;
    this.StreetAddress2 = userdetail.StreetAddress2 ;
    this.City = userdetail.City ;
    this.State = userdetail.State ;
    this.PostalCode = userdetail.PostalCode ;
    this.Country = userdetail.Country ;
    this.Telephone = userdetail.Telephone ;
    this.TempUserID = userdetail.TempUserID ;
    this.UserID = userdetail.UserID ;

    

};
UserDetail.createuserdetail = function createUser(userdetail, result) {
   
    var connection = new sql.Request();
    connection.query("EXEC Insupd_UserDetail '"+ userdetail.UserDetailID +"','"+ userdetail.FirstName +"','"+ userdetail.LastName  +"','"+userdetail.EmailAddress  +"','"+ userdetail.Address +"','"+ userdetail.StreetAddress2  +"','"+userdetail.City +"','"+ userdetail.State  +"','"+ userdetail.PostalCode +"','"+userdetail.Country +"','"+userdetail.Telephone +"','"+userdetail.TempUserID +"'" ,function (err, res) {             
        if(err) {
            
            result(err, null);
        }
        else{
           
            result(null, res);
        }
    });   
};

UserDetail.getuserdetail= function createUser(userdetail, result) {
   
    var connection = new sql.Request();
    connection.query("EXEC FetchUserDetail '"+ userdetail.UserDetailID +"'", function (err, res) {             
        if(err) {
            
            result(err, null);
        }
        else{
           
            result(null, res);
        }
    });   
};


UserDetail.getuserdetailbyid= function createUser(userdetailbyid, result) {
   
    var connection = new sql.Request();
    connection.query("EXEC Fetchuserdetailbyid '"+ userdetailbyid.UserID +"'", function (err, res) {             
        if(err) {
            
            result(err, null);
        }
        else{
           
            result(null, res);
        }
    });   
};

UserDetail.getUserCartbyId = function createUser(UserCartbyId, result) {
   
    var connection = new sql.Request();
    connection.query("EXEC FetchCartItemByUserId "+UserCartbyId.UserID+"", function (err, res) {             
        if(err) {
            
            result(err, null);
        }
        else{
           
            result(null, res);
        }
    });   
};

module.exports= UserDetail;