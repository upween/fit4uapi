'user strict';
var sql = require('./db.js');
//Category object constructor -- category_master
var   Orders = function(orders){
   
   this.OrderID  = orders.OrderID ;
   this.UserID  = orders.UserID ;
   this.Subtotal  = orders.Subtotal ;
   this.ShippingCharge  = orders.ShippingCharge ;
   this.TotalAmount   = orders.TotalAmount  ;
   this.PaymentMode   = orders.PaymentMode  ;
   this.Address =orders.Address ;
   this.OrderDetails=orders.OrderDetails;
   this.Status=orders.Status;
   this.ReferenceID=orders.ReferenceID;
};

Orders.createorders = function createUser(Orders, result) {
   
    var connection = new sql.Request();
    connection.query("EXEC Insupd_Orders "+Orders.OrderID  +","+Orders.UserID  +","+Orders.Subtotal  +",'"+Orders.ShippingCharge   +"','"+Orders.TotalAmount  +"','"+Orders.PaymentMode  +"','"+Orders.Address  +"','"+Orders.Status+"','"+Orders.ReferenceID+"'", function (err, res) {             
        if(err) {
            
            result(err, null);
        }
        else{
           
            result(null, res);
            var orderID = res.recordset[0].OrderID;
            const tvp = new sql.Table();
            tvp.columns.add('ORDER_ID', sql.Int);
            tvp.columns.add('PRODUCT_ID', sql.Int);
            tvp.columns.add('QUANTITY', sql.Int);

            var orderDetails = Orders.OrderDetails;
           
            for(var i = 0; i < orderDetails.length; i++) {
                tvp.rows.add(orderID, orderDetails[i].PRODUCT_ID, orderDetails[i].QUANTITY);
            }
            connection = new sql.Request();
            connection.input('details', tvp);
            connection.execute("InsOrderDetails", function (err1, res1) {
                console.log("Error: " + err1);
               // console.log("Response: " + res1);
            });
        }
    });   
};


Orders.getorders= function createUser(orders, result) {
   
    var connection = new sql.Request();
    connection.query("EXEC FetchOrders '"+ orders.OrderID  +"'", function (err, res) {             
        if(err) {
            
            result(err, null);
        }
        else{
           
            result(null, res);
        }
    });   
};

Orders.getorderbyuserid  = function createUser(orderbyuserid, result) {
   
    var connection = new sql.Request();
    connection.query("EXEC FetchOrdersByUserID  '"+orderbyuserid.UserID  +"'", function (err, res) {             
        if(err) {
            
            result(err, null);
        }
        else{
           
            result(null, res);
        }
    });   
};
Orders.getorderInvoice  = function createUser(orderInvoice, result) {
   
    var connection = new sql.Request();
    connection.query("EXEC FetchInvoice  '"+orderInvoice.OrderID  +"'", function (err, res) {             
        if(err) {
            
            result(err, null);
        }
        else{
           
            result(null, res);
        }
    });  
};  
module.exports= Orders;