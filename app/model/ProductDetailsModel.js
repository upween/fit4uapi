'user strict';
var sql = require('./db.js');
//Category object constructor -- category_master
var   ProductDetails = function(productdetails){
    this.ProductName = productdetails.ProductName;
    this.Description= productdetails.Description;
    this.Flavours = productdetails.Flavours;
    this.Price = productdetails.Price;
    this.Vegeterian= productdetails.Vegeterian;
    this.Image1 = productdetails.Image1;
    this.Image2 = productdetails.Image2;
    this.Image3 = productdetails.Image3;
    this.IsActive = productdetails.IsActive;
    this.Brand = productdetails.Brand;
    this.AmountAvailable = productdetails.AmountAvailable;
    this.GoalID = productdetails.GoalID;
    this.CategoryID = productdetails.CategoryID;
     this.UniqueID  = productdetails.UniqueID ;
    this.UserID  = productdetails.UserID ;
    this.Product_ID  = productdetails.Product_ID ;
    this.Quantity  = productdetails.Quantity ;
    this.EntryFrom  = productdetails.EntryFrom ;
    this.DiscountedPrice  = productdetails.DiscountedPrice ;
    this.Weight  = productdetails.Weight ;
    this.Id   = productdetails.Id  ;
    this.ParentID   = productdetails.ParentID  ;
    this.Type  = productdetails.Type ;


    

};
ProductDetails.createProductDetails = function createUser(productdetails, result) {
   
    var connection = new sql.Request();
    connection.query("EXEC Insert_ProductDetails '"+ productdetails.ProductName+"','"+ productdetails.Description+"','"+ productdetails.Flavours +"','"+productdetails.Price +"','"+ productdetails.Vegeterian+"','"+ productdetails.Image1 +"','"+productdetails.Image2+"','"+ productdetails.Image3 +"','"+ productdetails.IsActive+"','"+productdetails.Brand+"','"+productdetails.AmountAvailable+"','"+productdetails.GoalID+"','"+productdetails.CategoryID+"','"+productdetails.Weight+"'" ,function (err, res) {             
        if(err) {
            
            result(err, null);
        }
        else{
           
            result(null, res);
        }
    });   
};
ProductDetails.getmaxproductid = function createUser(maxproductid, result) {
   
    var connection = new sql.Request();
    connection.query("EXEC FetchMaxProductID", function (err, res) {             
        if(err) {
            
            result(err, null);
        }
        else{
           
            result(null, res);
        }
    });   
};

ProductDetails.createUserCartItems = function createUser(UserCartItems, result) {
   
    var connection = new sql.Request();
    connection.query("EXEC Insupd_UserCartItems '"+ UserCartItems.UniqueID +"','"+ UserCartItems.UserID +"','"+ UserCartItems.Product_ID  +"','"+UserCartItems.Quantity  +"','"+ UserCartItems.EntryFrom +"','"+ UserCartItems.Price  +"','"+UserCartItems.DiscountedPrice +"'" ,function (err, res) {             
        if(err) {
            
            result(err, null);
        }
        else{
           
            result(null, res);
        }
    });   
};


ProductDetails.getproductdetails = function createUser(productdetails, result) {
   
    var connection = new sql.Request();
    connection.query("EXEC FetchProductDetails '"+ productdetails.ProductID +"'", function (err, res) {             
        if(err) {
            
            result(err, null);
        }
        else{
           
            result(null, res);
        }
    });   
};



ProductDetails.getProductByGoalsAndBrand = function createUser(ProductByGoalsAndBrand, result) {
   
    var connection = new sql.Request();
    connection.query("EXEC fetchProductByGoalsAndBrand '"+ ProductByGoalsAndBrand.Id  +"','"+ ProductByGoalsAndBrand.ParentID +"','"+ ProductByGoalsAndBrand.Type +"'", function (err, res) {             
        if(err) {
            
            result(err, null);
        }
        else{
           
            result(null, res);
        }
    });   
};

ProductDetails.getProductbyid = function createUser(productbyid, result) {
   
    var connection = new sql.Request();
    connection.query("EXEC Fetchproductbyid '"+productbyid.ProductID+"'", function (err, res) {             
        if(err) {
            
            result(err, null);
        }
        else{
           
            result(null, res);
        }
    });   
};
ProductDetails.deleteCartItems = function createUser(deletecartitem, result) {
   
    var connection = new sql.Request();
    connection.query("EXEC DeleteCartItems "+deletecartitem.UserID+","+deletecartitem.UniqueID+"", function (err, res) {             
        if(err) {
            
            result(err, null);
        }
        else{
           
            result(null, res);
        }
    });   
};
ProductDetails.getlatestproduct = function createUser(latestproduct, result) {
   
    var connection = new sql.Request();
    connection.query("EXEC FetchLatestProduct '"+latestproduct.ProductID+"','"+latestproduct.ApplicationID+"','"+latestproduct.Limit+"'", function (err, res) {             
        if(err) {
            
            result(err, null);
        }
        else{
           
            result(null, res);
        }
    });   
};
module.exports= ProductDetails;