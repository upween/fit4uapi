'user strict';
var sql = require('./db.js');
//Category object constructor -- category_master
var   Masters = function(masters){
    this.CategoryID  = masters.CategoryID ;
    this.BrandID = masters.BrandID;
    this.GoalID  = masters.GoalID ;
    this.FlavourID  = masters.FlavourID ;
};

Masters.getCategory = function createUser(category, result) {
   
    var connection = new sql.Request();
    connection.query("EXEC FetchCategoryMaster "+category.CategoryID+"", function (err, res) {             
        if(err) {
            
            result(err, null);
        }
        else{
           
            result(null, res);
        }
    });   
};
Masters.getBrand= function createUser(brand, result) {
   
    var connection = new sql.Request();
    connection.query("EXEC FetchBrandMaster "+brand.BrandID+"", function (err, res) {             
        if(err) {
            
            result(err, null);
        }
        else{
           
            result(null, res);
        }
    });   
};
Masters.getGoals = function createUser(goals, result) {
   
    var connection = new sql.Request();
    connection.query("EXEC FetchGoalMaster "+goals.GoalID+"", function (err, res) {             
        if(err) {
            
            result(err, null);
        }
        else{
           
            result(null, res);
        }
    });   
};
Masters.getFlavour = function createUser(flavour, result) {
   
    var connection = new sql.Request();
    connection.query("EXEC FetchFlavourMaster "+flavour.FlavourID+"", function (err, res) {             
        if(err) {
            
            result(err, null);
        }
        else{
           
            result(null, res);
        }
    });   
};
module.exports= Masters;